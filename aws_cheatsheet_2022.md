# AWS Cheatsheet 2022
* [https://github.com/aws](https://github.com/aws)

# AWS Training and Certification
* [https://explore.skillbuilder.aws](https://explore.skillbuilder.aws)

# AWS Educate
* [https://www.awseducate.com](https://www.awseducate.com)

# AWS CloudFormation Infastructure As Code (IaC)
* [https://aws.amazon.com/cloudformation/](https://aws.amazon.com/cloudformation/)

# CDK
* [https://aws.amazon.com/getting-started/guides/setup-cdk/](https://aws.amazon.com/getting-started/guides/setup-cdk/)
* [https://github.com/aws/aws-cdk](https://github.com/aws/aws-cdk)
* [https://pypi.org/search/?q=aws-cdk](https://pypi.org/search/?q=aws-cdk)
* [https://aws.amazon.com/getting-started/guides/setup-cdk/module-three/](https://aws.amazon.com/getting-started/guides/setup-cdk/module-three/)
* [https://docs.aws.amazon.com/cdk/api/v1/docs/aws-construct-library.html](https://docs.aws.amazon.com/cdk/api/v1/docs/aws-construct-library.html)

Creating a AWS CDK project with Python

```bash
mkdir my-project
cd my-project
cdk init app --language python
```

# AWS CLI
* [https://aws.amazon.com/cli/](https://aws.amazon.com/cli/)
* [https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
* [https://github.com/aws/aws-cli](https://github.com/aws/aws-cli)

# AWS-SHELL
* [https://github.com/awslabs/aws-shell](https://github.com/awslabs/aws-shell)

